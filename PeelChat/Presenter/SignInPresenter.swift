//
//  SignInPresenter.swift
//  PeelChat
//
//  Created by Gone on 4/3/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit
import FirebaseDatabase

class SignInPresenter: NSObject {
    weak var signInDelegate: SignInDelegate?
    
    func getVerificationID(isCall: Bool, phoneNumber: String) {
        if isCall {
            DataManager.shared.getVerificationID(phoneNumber: phoneNumber, completionBlock: { [weak self] verificationID in
                guard let `self` = self else { return }
                self.signInDelegate?.getVerifyIDSuccess(verificationID: verificationID)
            }) { (error) in
                self.signInDelegate?.getVerifyIDFailed()
            }
        }
    }
    
    func signInAndRetrieveData(isCall: Bool, phoneNumber: String, password: String) {
        if isCall {
            DataManager.shared.getOrRetrieveObserveSingleEvent(path: Constants.userGlobalPath, childPath: phoneNumber, { [weak self] snapshot in
                guard let `self` = self else { return }
                print(snapshot)
                guard let user = UserGlobal(snapshot: snapshot) else { return }
                let ticks = Date().ticks
                guard let lastestTime = UInt64(user.lastTimeActive) else { return }
                let currTimeActive = ticks - lastestTime
                
                /// If login lastest time to current is > 1 hour, then we'll reset login count
                if user.password == password {
                    if currTimeActive > UInt64(3.6*pow(10, 9)/0.1) {
                        snapshot.ref.updateChildValues(["loginCount": 0])
                    }
                    snapshot.ref.updateChildValues(["lastTimeActive": String(ticks)])
                    DataManager.shared.getOrRetrieveObserveSingleEvent(path: Constants.userGlobalPath, childPath: phoneNumber, { [weak self] snapshot in
                        guard let `self` = self else { return }
                        guard let users = UserGlobal(snapshot: snapshot) else { return }
                        if user.loginCount <= 5 {
                            snapshot.ref.updateChildValues(["loginCount": users.loginCount+1])
                            self.signInDelegate?.singinSuccess()
                        } else {
                            self.signInDelegate?.signinFailed(message: TextGlobal.messageSignInOverflow)
                        }
                        }, { (_) in })
                } else {
                    self.signInDelegate?.signinFailed(message: TextGlobal.messagePaswordInvalid)
                }
            }) { (error) in
                self.signInDelegate?.signinFailed(message: TextGlobal.messagePaswordInvalid)
            }
        }
    }
}
