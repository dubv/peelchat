//
//  VerifyPresenter.swift
//  PeelChat
//
//  Created by Gone on 4/4/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

class VerifyPresenter: NSObject {
    weak var verifyDelegate: VerifyDelegate?
    
    func sigInWithVerificationID(verificationID: String, verificationCode: String) {
        DataManager.shared.signInAndRetrieve(verificationID: verificationID, verificationCode: verificationCode, completionBlock: { [weak self] uid in
            guard let `self` = self else { return }
            self.verifyDelegate?.signinWithVerificationIDSuccess(uid)
            }, fail: { (error) in
                self.verifyDelegate?.signinWithVerificationIDFailed()
        })
    }
}
