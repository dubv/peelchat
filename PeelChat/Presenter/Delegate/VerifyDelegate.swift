//
//  VerifyDelegate.swift
//  PeelChat
//
//  Created by Gone on 4/4/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

protocol VerifyDelegate: NSObjectProtocol {
    func signinWithVerificationIDSuccess(_ uid: String)
    func signinWithVerificationIDFailed()
}

extension VerifyDelegate {
    public func signinWithVerificationIDSuccess(_ uid: String) { }
    public func signinWithVerificationIDFailed() { }
}
