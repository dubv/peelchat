//
//  HomeDelegate.swift
//  PeelChat
//
//  Created by Gone on 4/4/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

protocol HomeDelegate: NSObjectProtocol {
    func getUserSuccess(listUser: [User]?)
    func getUserFailed()
}

extension HomeDelegate {
    public func getUserSuccess(listUser: [User]?) { }
    public func getUserFailed() { }
}
