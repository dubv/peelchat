//
//  SignInDelegate.swift
//  PeelChat
//
//  Created by Gone on 4/3/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

protocol SignInDelegate: NSObjectProtocol {
    func singinSuccess()
    func signinFailed(message: String)
    func getVerifyIDSuccess(verificationID: String)
    func getVerifyIDFailed()
}

extension SignInDelegate {
    public func singinSuccess() { }
    public func signinFailed(message: String) { }
    public func getVerifyIDSuccess(verificationID: String) { }
    public func getVerifyIDFailed() { }
}
