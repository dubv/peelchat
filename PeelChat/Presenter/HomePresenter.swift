//
//  HomePresenter.swift
//  PeelChat
//
//  Created by Gone on 4/4/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit
import FirebaseDatabase

class HomePresenter: NSObject {
    weak var homeDelegate: HomeDelegate?
    
    func getListUser() {
        DataManager.shared.getOrRetrieveObserveSingleEvent(path: Constants.userPath, { [weak self] snapshot in
            guard let `self` = self else { return }
            var listUser = [User]()
            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot, let user = User(snapshot: snapshot) {
                    listUser.append(user)
                }
            }
            self.homeDelegate?.getUserSuccess(listUser: listUser)
            print(snapshot)
        }) { (_) in
            self.homeDelegate?.getUserFailed()
        }
    }
}
