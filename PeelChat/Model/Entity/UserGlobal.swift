//
//  UserGlobal.swift
//  ThePeel
//
//  Created by Gone on 3/1/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class UserGlobal: NSObject {
    var ref: DatabaseReference?
    var key: String?
    var uid: String
    var password: String
    var lastTimeActive: String
    var loginCount: Int
    var userToken: String
    
    init(key: String, uid: String, password: String, lastTimeActive: String, loginCount: Int, userToken: String) {
        self.key = key
        self.uid = uid
        self.password = password
        self.lastTimeActive = lastTimeActive
        self.loginCount = loginCount
        self.userToken = userToken
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: AnyObject],
            let uid = value["uid"] as? String,
            let password = value["password"] as? String,
            let lastTimeActive = value["lastTimeActive"] as? String,
            let loginCount = value["loginCount"] as? Int,
            let userToken = value["user_token"] as? String
            else { return nil }
        
        self.ref = snapshot.ref
        self.key = ref?.key
        self.uid = uid
        self.password = password
        self.lastTimeActive = lastTimeActive
        self.loginCount = loginCount
        self.userToken = userToken
    }
    
    func toAnyObject() -> Any {
        return [
            "uid": uid,
            "lastTimeActive": lastTimeActive,
            "password": password,
            "loginCount": loginCount,
            "user_token": userToken
        ]
    }
}
