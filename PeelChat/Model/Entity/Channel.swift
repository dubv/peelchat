//
//  Chanel.swift
//  PeelChat
//
//  Created by Gone on 4/2/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import FirebaseDatabase

class Channel: NSObject {
    var ref: DatabaseReference?
    var channelId: String
    var channelName: String
    var messager: [Messager]
    
    init(channelId: String, channelName: String, messager: [Messager]) {
        self.channelId = channelId
        self.channelName = channelName
        self.messager = messager
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: Any],
            let channelId = value["channelId"] as? String,
            let channelName = value["channelName"] as? String else { return nil }
        
        var messagesList = [Messager]()
        for child in snapshot.children {
            if let snapshot = child as? DataSnapshot{
                for child in snapshot.children {
                    if let snapshot = child as? DataSnapshot, let message = Messager(snapshot: snapshot) {
                        messagesList.append(message)
                    }
                }
            }
        }
        
        self.ref = snapshot.ref
        self.channelId = channelId
        self.channelName = channelName
        self.messager = messagesList
    }
    
    func toAnyObject() -> Any {
        return [
            "channelId": channelId,
            "channelName": channelName,
            "messager": [messager]
        ]
    }
}

class Messager: NSObject {
    var senderID: String
    var senderName: String
    var created: String
    var content: String
    
    init(senderID: String, senderName: String, created: String, content: String) {
        self.senderID = senderID
        self.senderName = senderName
        self.created = created
        self.content = content
    }
    
    init?(snapshot: DataSnapshot) {
        guard let value = snapshot.value as? [String: Any],
            let senderID = value["senderId"] as? String,
            let senderName = value["senderName"] as? String,
            let created = value["created"] as? String,
            let content = value["content"] as? String else { return nil }
        
        self.senderID = senderID
        self.senderName = senderName
        self.created = created
        self.content = content
    }
    
    func toAnyObject() -> Any {
        return [
            "senderID": senderID,
            "senderName": senderName,
            "created": created,
            "content": content
        ]
    }
}
