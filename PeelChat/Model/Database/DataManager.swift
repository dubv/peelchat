//
//  DataManager.swift
//  PeelChat
//
//  Created by Gone on 4/3/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit
import Firebase

class DataManager: NSObject {
    static let shared = DataManager()
    private var ref: Database!
    private var auth: Auth!
    
    private override init() {
        Auth.auth().languageCode = "en-US"
        Database.database().isPersistenceEnabled = true
        ref = Database.database()
    }
    
    func getOrRetrieveObserveSingleEvent(path: String, childPath: String? = nil, _ completionBlock: @escaping (DataSnapshot) -> Void, _ fail: @escaping (String) -> Void){
        if let childPath = childPath {
            ref.reference(withPath: path).child(childPath).observeSingleEvent(of: .value, with: { snapshot in
                if snapshot.exists() {
                    completionBlock(snapshot)
                } else {
                    fail(TextGlobal.messageErrorUnknown)
                }
            })
        } else {
            ref.reference(withPath: path).observeSingleEvent(of: .value, with: { snapshot in
                if snapshot.exists() {
                    completionBlock(snapshot)
                } else {
                    fail(TextGlobal.messageErrorUnknown)
                }
            })
        }
    }
    
    /// Do get verificationID in signin/siginup
    func getVerificationID(phoneNumber: String, completionBlock: @escaping (String) -> Void, fail: ((NSError) -> Void)?) {
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil, completion: { (verificationID, error) in
            if let error = error {
                fail?(error as NSError)
                return
            }
            guard let verificationID = verificationID else { return }
            completionBlock(verificationID)
        })
    }
    
    /// Do signin with verificationID
    func signInAndRetrieve(verificationID: String, verificationCode: String, completionBlock: @escaping (String) -> Void, fail: ((NSError) -> Void)?) {
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID,
            verificationCode: verificationCode)
        
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error{
                fail?(error as NSError)
                return
            }
            guard let user = authResult?.user else { return }
            completionBlock(user.uid)
        }
    }
}
