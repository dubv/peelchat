//
//  Pincode+Extension.swift
//  PeelChat
//
//  Created by Gone on 4/3/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

protocol PinCodeTextFieldDelegate: UITextFieldDelegate {
    func didPressBackspace(textField : PinCodeTextField)
}

class PinCodeTextField: UITextField {
    
    // MARK: - Properties
    weak var pinCodeTextFieldDelegate: PinCodeTextFieldDelegate?
    
    // MARK: - Life Cycle
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.keyboardType = .numberPad
        self.layer.cornerRadius = 3.0
    }
    
    override func deleteBackward() {
        super.deleteBackward()
        pinCodeTextFieldDelegate?.didPressBackspace(textField: self)
    }
    
    // MARK: - Private Method
    private func generateToolbar() -> UIToolbar {
        let width = UIScreen.main.bounds.width
        let toolbar =  UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: width, height: 60.0))
        let itme1 = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let item2 = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneButtonDidPressed))
        toolbar.setItems([itme1, item2], animated: false)
        return toolbar
    }
    
    // MARK: - Target
    @objc func doneButtonDidPressed() {
        self.resignFirstResponder()
    }
}
