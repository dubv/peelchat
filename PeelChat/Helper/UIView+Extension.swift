//
//  UIView+Extension.swift
//  PeelChat
//
//  Created by Gone on 4/4/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

public extension UIView {
    
    /// A common public function for loading view from nib
    ///
    /// - Returns: a view
    @discardableResult
    func fromNib<T: UIView>() -> T? {
        guard let view = UINib(nibName: String(describing: type(of: self)), bundle: nil).instantiate(withOwner: self, options: nil).first as? T else {
            return nil
        }
        
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(view)
        return view
    }
    
    class func viewFromNib(withOwner owner: Any? = nil) -> Self {
        let name = String(describing: type(of: self)).components(separatedBy: ".")[0]
        let view = UINib(nibName: name, bundle: nil).instantiate(withOwner: owner, options: nil)[0]
        return cast(view)!
    }
    
    func loadFromNibIfEmbeddedInDifferentNib() -> Self {
        let isJustAPlaceholder = subviews.count == 0
        if isJustAPlaceholder {
            let theRealThing = type(of: self).viewFromNib()
            theRealThing.frame = frame
            translatesAutoresizingMaskIntoConstraints = false
            theRealThing.translatesAutoresizingMaskIntoConstraints = false
            return theRealThing
        }
        return self
    }
}

private func cast<T, U>(_ value: T) -> U? {
    return value as? U
}
