//
//  Localizable+Extension.swift
//  PeelChat
//
//  Created by quy nguyen on 9/11/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation

let LangIdentifier = "ja"

extension String {
    
    var localized : String{
        return localizeString(LangIdentifier)
    }
    
    func localizeString(_ lang: String? = "ja") ->String {
        if let path = Bundle.main.path(forResource: lang, ofType: "lproj"){
            let bundle = Bundle(path: path)
            return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
        }
        return NSLocalizedString(self, comment: "")
    }
    
}
