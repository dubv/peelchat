//
//  Language.swift
//  PeelChat
//
//  Created by Gone on 4/3/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class TextGlobal {
    
    // MARK: - CONTROL
    static var  buttonOk: String { return "BUTTON_OK".localized }
    static var  buttonCancel: String {return "BUTTON_CANCEL".localized }
    
    // MARK: - MESSAGE COMMON
    static var  messageNoInternet: String { return "MESSAGE_NO_INTERNET".localized }
    static var  messageErrorURL: String {return "MESSAGE_ERROR_URL".localized }
    static var  messageErrorUnknown: String {return "MESSAGE_ERROR_UNKNOWN".localized }
    static var  messageErrorObjectSericalization: String {return "MESSAGE_ERROR_OBJECT_SERICALIZATION".localized }
    static var  messageErrorParseJson: String {return "MESSAGE_ERROR_PARSE_JSON".localized }
    static var  messageErrorCallService: String { return "MESSAGE_ERROR_CALL_SERVICE".localized }
    
    // MARK: - PLACE HOLDER
    static var  placeholderPhoneNumber: String { return "PHONE_NUMBER".localized }
    static var  placeholderPassword: String { return "PASSWORD".localized }
    
    // MARK: - ALERT TITLE
    static var  titleError: String { return "ERROR".localized }
    
    // MARK: - SIGNIN
    static var  messagePaswordInvalid: String { return "MESSAGE_PASSWORD_WRONG".localized }
    static var  messagePhoneNumberInvalid: String { return "MESSAGE_PHONE_NUMBER_INVALID".localized }
    static var  messageVertificationIDInvalid: String { return "MESSAGE_VERIFICATION_ID_INCORRECT".localized }
    static var  messageSignInOverflow: String { return "MESSAGE_SIGNIN_OVERFLOW".localized }
    
    // MARK: - NAVIGATION TITLE
    static var  titleMessages: String { return "TITLE_MESSAGES".localized }
}
