//
//  Constants.swift
//  PeelChat
//
//  Created by Gone on 4/3/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

public class Constants: NSObject {
    static var numberPhonePrefix = "+84"
    
    /// Firebase ref
    static var userGlobalPath = "userglobal"
    static var userPath = "user"
}
