//
//  AppColor.swift
//  PeelChat
//
//  Created by Gone on 4/4/19.
//  Copyright © 2019 studio4. All rights reserved.
//
import UIKit

class AppColor {
    static let backgroundColor: UIColor = UIColor(hexString: "#00a0dc")
    static let mainColor: UIColor = UIColor(hexString: "#008D4C")
    static let textMainColor: UIColor = UIColor(hexString: "#333333")
    static let placeHolderColor: UIColor = UIColor(hexString: "#DDDDDD")
    static let backgroudTextInputColor: UIColor = UIColor(hexString: "#FDFCFA")
    static let requireBackgroundColor: UIColor = UIColor(hexString: "#FFFFF5")
    static let requireBorderColor: UIColor = UIColor(hexString: "#00A0E9")
    static let messageInputBorderColor: UIColor = UIColor(hexString: "#D2D4D5")
}
