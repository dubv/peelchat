//
//  Utilities.swift
//  PeelChat
//
//  Created by Gone on 4/2/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

enum UUID {
    ///storedTypeProperty
    static var uid: String?
    ///computedTypeProperty
    static var uuid: String? {
        return uid
    }
}
