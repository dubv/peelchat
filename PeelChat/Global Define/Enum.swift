//
//  Enum.swift
//  Studio4FrameMVP
//
//  Created by quy nguyen on 8/24/18.
//  Copyright © 2018 studio4. All rights reserved.
//

import Foundation

enum RequestType: Int {
    case requestTypeGet = 1, requestTypePost, requestTypePut, requestTypeDelete
}

enum ModelError: Int, Error {
    case urlError = 400
    case unknown  = 404
    case noInternet = 401
    case objectSericalization = 405
    case parseJson = 505
    case serviceError = 500
    
    var localizedDescription: String {
        switch self {
        case .urlError:
            return TextGlobal.messageErrorURL
        case .unknown:
            return TextGlobal.messageErrorUnknown
        case .noInternet:
            return TextGlobal.messageNoInternet
        case .objectSericalization:
            return TextGlobal.messageErrorObjectSericalization
        case .parseJson:
            return TextGlobal.messageErrorParseJson
        case .serviceError:
            return TextGlobal.messageErrorCallService
        }
    }
    var code: Int {
        return self.rawValue
    }
}
