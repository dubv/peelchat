//
//  ChatTableViewCell.swift
//  PeelChat
//
//  Created by Gone on 4/2/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var mainContainView: UIView!
    @IBOutlet fileprivate weak var containMessageView: UIView!
    @IBOutlet fileprivate weak var contentLabel: UILabel!
    
    // MARK: - Properties
    public var messages: Messager? {
        didSet {
//            guard let message = messages else { return }
        }
    }
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI(constrain: mainContainView.rightAnchor, constant: -10, bgColor: "F5F5F5", borderWidth: 0, borderColor: "FFFFFF")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
    
    // MARK: - Private Method
    public func setupMessage(message: Messager) {
        if let uuid = UUID.uuid, message.senderID == uuid {
        }
    }
    
    private func setupUI(constrain: NSLayoutAnchor<NSLayoutXAxisAnchor>, constant: CGFloat, bgColor: String, borderWidth: CGFloat, borderColor: String) {
        mainContainView.addSubview(containMessageView)
        containMessageView.translatesAutoresizingMaskIntoConstraints = false
        containMessageView.rightAnchor.constraint(equalTo: constrain, constant: constant).isActive = true
        containMessageView.topAnchor.constraint(equalTo: mainContainView.topAnchor, constant: 5).isActive = true
        containMessageView.bottomAnchor.constraint(equalTo: mainContainView.bottomAnchor, constant: -5).isActive = true
        containMessageView.layer.cornerRadius = containMessageView.frame.height/3
        containMessageView.backgroundColor = UIColor(hexString: bgColor)
        containMessageView.layer.borderColor = UIColor(hexString: borderColor).cgColor
        containMessageView.layer.borderWidth = borderWidth
        
        containMessageView.addSubview(contentLabel)
        contentLabel.translatesAutoresizingMaskIntoConstraints = false
        contentLabel.topAnchor.constraint(equalTo: containMessageView.topAnchor, constant: 10).isActive =  true
        contentLabel.rightAnchor.constraint(equalTo: containMessageView.rightAnchor, constant: -10).isActive =  true
        contentLabel.bottomAnchor.constraint(equalTo: containMessageView.bottomAnchor, constant: -10).isActive =  true
        contentLabel.leftAnchor.constraint(equalTo: containMessageView.leftAnchor, constant: 10).isActive =  true
    }
    
    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction
    // MARK: - Service
    
}
