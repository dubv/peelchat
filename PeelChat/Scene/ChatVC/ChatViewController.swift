//
//  ChatViewController.swift
//  PeelChat
//
//  Created by Gone on 4/2/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

enum Messaging {
    case owner
    case receiver
}

class ChatViewController: BaseVC {
    
    // MARK: - IBOutlet
    @IBOutlet fileprivate weak var inputMessageView: UIView!
    @IBOutlet fileprivate weak var inputMessageTextView: UITextView!
    @IBOutlet fileprivate weak var textViewHeightConstrain: NSLayoutConstraint!
    
    // MARK: - Properties
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        inputMessageTextView.delegate = self
        setupUI()
    }
    
    // MARK: - Initialization
    
    // MARK: - Private Method
    private func setupUI() {
        inputMessageTextView.placeholder = "Message"
        inputMessageView.layer.cornerRadius = inputMessageView.frame.height/2.1
        inputMessageView.layer.borderWidth = 1
        inputMessageView.layer.borderColor = AppColor.messageInputBorderColor.cgColor
    }
    
    private func registerCell() {
        
    }
    
    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction
    // MARK: - Service
}

extension ChatViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        let size = CGSize(width: view.frame.width, height: .infinity)
        let estimatedSize = textView.sizeThatFits(size)
        
        if let placeholderLabel = inputMessageTextView.viewWithTag(100) as? UILabel {
            placeholderLabel.isHidden = !inputMessageTextView.text.isEmpty
        }
        
        UIView.animate(withDuration: 0.5, animations: { () in
            self.textViewHeightConstrain.constant = estimatedSize.height
//            self.inputMessageTextView.text.isEmpty ? (self.postButton.isHidden = true) : (self.postButton.isHidden = false)
        })
    }
}

// MARK: - UIImagePickerControllerDelegate
extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
}
