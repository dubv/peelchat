//
//  SignInViewController.swift
//  PeelChat
//
//  Created by Gone on 4/3/19.
//  Copyright © 2019 studio4. All rights reserved. 
//

import UIKit

class SignInViewController: BaseVC {
    
    // MARK: - IBOutlet
    @IBOutlet private weak var formContainView: AGRoundCornersView!
    @IBOutlet private weak var phoneNumberTV: UITextView!
    @IBOutlet private weak var passwordTV: UITextView!
    @IBOutlet private weak var signInButtonView: UIView!
    
    // MARK: - Properties
    private var signinPresenter = SignInPresenter()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        showProgress(false)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - Initialization Method
    private func setupUI() {
        formContainView.roundCorner(with: [.topLeft(10), .topRight(10)])
        signInButtonView.layer.cornerRadius = 5
        
        /// Setup TextView
        phoneNumberTV.placeholder = TextGlobal.placeholderPhoneNumber
        passwordTV.placeholder = TextGlobal.placeholderPassword
        phoneNumberTV.centerVertically()
        passwordTV.centerVertically()
    }
    
    // MARK: - Private Method
    private func isValidate() -> Bool {
        if !self.isNetworkAvailable() {
            return false
        }
        return true
    }
    
    // MARK: - Public Method
    // MARK: - Target
    
    // MARK: - IBAction
    @IBAction private func siginAction(_ sender: Any) {
        showProgress(true)
        if self.isValidate() {
            if let phoneNumber = phoneNumberTV.text, let password = passwordTV.text {
                signinPresenter.signInAndRetrieveData(isCall: true, phoneNumber: phoneNumber, password: password)
            }
        }
    }
    
    // MARK: - Service
}
// MARK: - Data
extension SignInViewController: SignInDelegate {
    
    // MARK: - SignInDelegate
    func singinSuccess() {
        showProgress(false)
        if let phoneNumber = phoneNumberTV.text {
            signinPresenter.getVerificationID(isCall: true, phoneNumber: Constants.numberPhonePrefix+phoneNumber)
        }
    }
    
    func signinFailed(message: String) {
        showProgress(false)
        showAlert(TextGlobal.titleError, message, titleButtonDone: TextGlobal.buttonOk)
    }
    
    func getVerifyIDSuccess(verificationID: String) {
        showProgress(false)
        UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
        let verifyVC = VerifyViewController()
        navigationController?.pushViewController(verifyVC, animated: true)
    }
    
    func getVerifyIDFailed() {
        showProgress(false)
        showAlert(TextGlobal.titleError, TextGlobal.messagePhoneNumberInvalid, titleButtonDone: TextGlobal.buttonOk, titleButtonCancel: nil, DoneAction: nil, CancelAction: nil)
    }
}
