
//
//  SignInNavigator.swift
//  PeelChat
//
//  Created by Gone on 4/4/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

protocol SignInNavigator {
    func toHome(window: UIWindow?)
}

class DefaultSignInNavigator: SignInNavigator {
    weak var navigation: UINavigationController?
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
    }
    
    func toSignIn(){
        let vc = SignInViewController()
        navigation?.pushViewController(vc, animated: true)
    }
    
    func toHome(window: UIWindow?) {
        let homeNavigation = UINavigationController()
        let homeNavigator = DefaultHomeNavigator(navigation: homeNavigation)
        
        if let window = window {
            
        }
    }
}
