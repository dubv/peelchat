//
//  VerifyViewController.swift
//  PeelChat
//
//  Created by Gone on 4/3/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

class VerifyViewController: BaseVC {
    
    // MARK: - IBOutlet
    @IBOutlet private weak var codeTextField6: PinCodeTextField!
    @IBOutlet private weak var codeTextField5: PinCodeTextField!
    @IBOutlet private weak var codeTextField4: PinCodeTextField!
    @IBOutlet private weak var codeTextField3: PinCodeTextField!
    @IBOutlet private weak var codeTextField2: PinCodeTextField!
    @IBOutlet private weak var codeTextField1: PinCodeTextField!
    @IBOutlet private weak var pincodeView: UIView!
    @IBOutlet private weak var verifyButtonView: UIView!

    // MARK: - Properties
    private var verifyPresenter = VerifyPresenter()
    private var currentTextField = UITextField()
    private var window: UIWindow?
    private var pinCode: String = ""
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupDelegate()
    }
    
    // MARK: - Initialization Method
    private func setupUI() {
        verifyButtonView.layer.cornerRadius = 5
        codeTextField1.becomeFirstResponder()
    }
    
    private func setupDelegate() {
        [codeTextField1, codeTextField2, codeTextField3, codeTextField4, codeTextField5, codeTextField6].forEach {
            $0?.delegate = self
            $0?.pinCodeTextFieldDelegate = self
        }
    }
    
    // MARK: - Private Method
    private func isValidate() -> Bool {
        if !self.isNetworkAvailable() {
            return false
        }
        return true
    }
    
    // MARK: - Public Method
    
    // MARK: - Target
    @objc fileprivate func textFieldEditing(_ textField: UITextField) {
        let nextResponder = textField.superview?.viewWithTag(textField.tag+1) as? UITextField
        if let nextRes = nextResponder {
            nextRes.becomeFirstResponder()
        }
    }
    
    // MARK: - IBAction
    @IBAction private func touchUpInsideVerify(_ sender: Any) {
        pinCode.removeAll()
        [codeTextField1, codeTextField2, codeTextField3, codeTextField4, codeTextField5, codeTextField6].forEach{
            if let text = $0?.text {
                pinCode += text
            }
        }
        
        if let verificationID = UserDefaults.standard.string(forKey: "authVerificationID") {
            if self.isValidate() {
                verifyPresenter.sigInWithVerificationID( verificationID: verificationID, verificationCode: pinCode)
            }
        }
    }
    
    // MARK: - Service
}
// MARK: - Data
extension VerifyViewController: VerifyDelegate {
    // MARK: - VerifyDelegate
    func signinWithVerificationIDSuccess(_ uid: String) {
        let window = UIWindow(frame: UIScreen.main.bounds)
        window.backgroundColor = .white
        self.window = window
        window.makeKeyAndVisible()
        
        let homeVC = HomeViewController()
        let nav = UINavigationController(rootViewController: homeVC)
        window.rootViewController = nav
    }
    
    func signinWithVerificationIDFailed() {
        showAlert(TextGlobal.titleError, TextGlobal.messageVertificationIDInvalid, titleButtonDone: TextGlobal.buttonOk)
    }
}
// MARK: - UITextFieldDelegate, PinCodeTextFieldDelegate
extension VerifyViewController: UITextFieldDelegate, PinCodeTextFieldDelegate {
    func didPressBackspace(textField: PinCodeTextField) {
        let previousResponder = textField.superview?.viewWithTag(textField.tag-1) as? UITextField
        if let previousRes = previousResponder{
            previousRes.becomeFirstResponder()
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.selectAll(nil)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                textField.removeTarget(self, action: #selector(textFieldEditing(_:)), for: .editingChanged)
            } else {
                textField.addTarget(self, action: #selector(textFieldEditing(_:)), for: .editingChanged)
            }
        }
        
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        textField.isSelected = true
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 1
    }
}
