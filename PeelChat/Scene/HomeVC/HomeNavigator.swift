//
//  HomeNavigator.swift
//  PeelChat
//
//  Created by Gone on 4/5/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

protocol HomeNavigator {
    func toHome()
}

struct DefaultHomeNavigator: HomeNavigator {
    weak var navigation: UINavigationController?
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
    }
    
    func toHome() {
        let homeVC = HomeViewController()
        navigation?.pushViewController(homeVC, animated: true)
    }
}
