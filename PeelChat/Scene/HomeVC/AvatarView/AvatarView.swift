//
//  AvatarView.swift
//  PeelChat
//
//  Created by Gone on 4/4/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

class AvatarView: UIView {
    
    @IBOutlet private weak var contentView: UIView!
    private var shouldSetupConstraints = true
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        fromNib()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        initView()
    }
    
    override func updateConstraints() {
        if(shouldSetupConstraints) {
            // AutoLayout constraints
            shouldSetupConstraints = false
        }
        super.updateConstraints()
    }
    
    func initView() {
        contentView.layer.cornerRadius = contentView.frame.height/2
    }
}
