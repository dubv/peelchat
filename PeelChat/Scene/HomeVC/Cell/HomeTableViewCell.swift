//
//  HomeTableViewCell.swift
//  PeelChat
//
//  Created by Gone on 4/2/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    // MARK: - IBOutlet
    @IBOutlet private weak var avatarContainView: UIView!
    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var userNameLabel: UILabel!
    @IBOutlet private weak var lastMessageLabel: UILabel!
    @IBOutlet private weak var dateTimeLabel: UILabel!
    
    // MARK: - Properties
    
    // MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Initialization Method
    private func setupUI() {
        avatarContainView.layer.cornerRadius = avatarContainView.frame.height/2
        avatarContainView.backgroundColor = .lightGray
    }
    
    // MARK: - Private Method
    // MARK: - Public Method
    // MARK: - Target
    // MARK: - IBAction
    // MARK: - Service
}
