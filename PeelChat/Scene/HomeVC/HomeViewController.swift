//
//  HomeViewController.swift
//  PeelChat
//
//  Created by Gone on 4/2/19.
//  Copyright © 2019 studio4. All rights reserved.
//

import UIKit

class HomeViewController: BaseVC {
    
    // MARK: - IBOutlet
    @IBOutlet private weak var chanelTableView: UITableView!
    
    // MARK: - Properties
    private var homePresenter = HomePresenter()
    private var listUser = [User]()
    private var search = UISearchController()
    private var avatarImageView = UIImageView()
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = TextGlobal.titleMessages
        registerCell()
        setupNavigationItem()
        serviceGetListUser()
    }
    
    // MARK: - Initialization Medthod
    
    // MARK: - Private Method
    private func isValidate() -> Bool {
        if !self.isNetworkAvailable() {
            return false
        }
        return true
    }
    
    private func setupNavigationItem() {
        // ---------------- Setup Searchbar ---------------- //
        search = UISearchController(searchResultsController: nil)
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            search.searchResultsUpdater = self
            navigationItem.searchController = search
        }
        search.becomeFirstResponder()
        search.obscuresBackgroundDuringPresentation = false
        
        // ---------------- Show date time ---------------- //
        let label = UILabel()
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE dd"
        label.text = dateFormatter.string(from: date).uppercased()
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: label)
        setupAvatarView()
    }
    
    private func setupAvatarView() {
        // ---------------- Set avatar label ---------------- //
        let avatarView = UIView(frame: CGRect(x: 0, y: 0, width: 35, height: 35))
        avatarView.backgroundColor = AppColor.requireBorderColor
        avatarView.layer.cornerRadius = avatarView.frame.height/2
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: avatarView)
    }
    
    private func registerCell() {
        chanelTableView.register(HomeTableViewCell.self)
    }
}
// MARK: - Data
extension HomeViewController: HomeDelegate {
    
    // MARK: - Get List
    private func serviceGetListUser() {
        if self.isValidate() {
            homePresenter.homeDelegate = self
            showProgress(true)
            homePresenter.getListUser()
        }
    }
    
    // MARK: - HomeDelegate
    func getUserSuccess(listUser: [User]?) {
        showProgress(false)
        guard let listUser = listUser, listUser.count > 0 else { return }
        self.listUser = listUser
    }
    
    func getUserFailed() {
        showProgress(false)
    }
}
// MARK: - UITableViewDelegate, UITableViewDataSource
extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(HomeTableViewCell.self, for: indexPath)
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let chatVC = ChatViewController()
        navigationController?.pushViewController(chatVC, animated: true)
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.5
    }
}
// MARK: - UISearchResultsUpdating
extension HomeViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
//        print(searchController.searchBar.text)
    }
}
