//
//  Application.swift
//  PeelChat
//
//  Created by Gone on 4/3/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

struct Application {
    static let shared = Application()
    private init() {}
    
    func configMainInterface(window: UIWindow) {
        let uid = Auth.auth().currentUser?.uid
        let signInVC = SignInViewController()
        let homeVC = HomeViewController()
        
        if uid != nil {
            setupRootView(window: window, viewController: homeVC)
        } else {
            setupRootView(window: window, viewController: signInVC)
        }
        
        window.makeKeyAndVisible()
    }
    
    func setupRootView(window: UIWindow, viewController: UIViewController) {
        let nav = UINavigationController(rootViewController: viewController)
        window.rootViewController = nav
    }
}
