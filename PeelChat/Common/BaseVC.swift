//
//  BaseVC.swift
//  PeelChat
//
//  Created by Gone on 4/2/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import SVProgressHUD

class BaseVC: UIViewController {
    
    // MARK: - Properties
    var navCustomView: UIView? {
        didSet {
            self.navigationItem.titleView = navCustomView
        }
    }
    
    var navTitle: String? {
        didSet {
            let titleLabel = UILabel.init(frame: CGRect.zero)
            titleLabel.font = UIFont.defaultFont(isBold: false)
            titleLabel.textColor = AppColor.backgroundColor
            titleLabel.text = navTitle
            titleLabel.sizeToFit()
            self.navigationItem.titleView = titleLabel
        }
    }
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    // MARK: - Initialization Method
    
    // MARK: - Private Method
    
    // MARK: - Public Method
    func setNavigationBar(title: String?, leftBarButton: [UIBarButtonItem]?, rightBarButton: [UIBarButtonItem]?) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.navigationController?.navigationBar.tintColor = AppColor.backgroundColor
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [ NSAttributedString.Key.foregroundColor: AppColor.backgroundColor]
        if let titleBar = title {
            self.navigationItem.title = titleBar
        }
        
        if let listLeftBarButton = leftBarButton {
            self.navigationItem.leftBarButtonItems = listLeftBarButton
        }
        
        if let listRightBarButton = rightBarButton {
            self.navigationItem.rightBarButtonItems = listRightBarButton
        }
    }
    
    func isNetworkAvailable() -> Bool {
        if Network.isConnectedToNetwork() == false {
            self.showAlert(nil, TextGlobal.messageNoInternet, titleButtonDone: TextGlobal.buttonOk, titleButtonCancel: nil)
            return false
        }
        return true
    }
    
    func showProgress(_ isShow: Bool) {
        SVProgressHUD.setDefaultMaskType(.clear)
        
        if isShow {
            //show
            DispatchQueue.main.async {
                SVProgressHUD.show()
            }
        } else {
            //hide
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
        }
    }
    
    // MARK: - Base Service
    func callServiceFail(error: ModelError) {
        self.showAlert(nil, error.localizedDescription, titleButtonDone: TextGlobal.buttonOk, titleButtonCancel: nil)
        self.showProgress(false)
    }
    
    // MARK: - Target
    
    // MARK: - IBAction
    
}
